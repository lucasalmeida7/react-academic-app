// expo-sqlite: 5.0.1

import { SQLite } from "expo-sqlite";

const table = 'CREATE TABLE IF NOT EXISTS Content (id integer primary key, name, desc, img)'
const insert = 'INSERT INTO Content VALUES (null, ?, ?, ?)';
const update = 'UPDATE Content SET name=?, desc=?, img=? WHERE id=?';
const select = 'SELECT c.id, c.name, c.img FROM Content c';
const selectbyid = 'SELECT c.id, c.name, c.img FROM Content c WHERE id=?';
const  DELETE = 'DELETE FROM Content WHERE id = ?';

export default class Database {

    initDB() {
        const db = SQLite.openDatabase('RN.db');
        db.transaction((tx) => {tx.executeSql(table)});
        return db;
    }

    closeDatabase(db) {
        db.close();
    }

    listContents(db) {
        return new Promise((resolve)=>{
            const contents =[];
            db.transaction((tx) => {
                     tx.executeSql(select, [],
                    (_, { rows })=>{
                        console.log(rows);
                        const len = rows.length;
                        for (let i =0; i< len; i++) {
                            const { id, name, img} = rows.item(i);
                            contents.push({
                                id, name, img
                            })
                        }
                        resolve(contents);
                    }, 
                    (error)=>{console.log('erro ao fazer o select ', error)})
                 }); 
        })
    }

    contentById(id, db) {
        return new Promise((resolve)=>{
            db.transaction((tx) =>      {
                     tx.executeSql(selectbyid, [id],
                    (_, { rows })=>{
                        console.log(rows);
                        resolve(rows.item(0));
                    }, 
                    (error)=>{console.log('erro ao fazer o selectbyid ', error)})
                 }); 
        })
    }

    addContent(content, db) {
        db.transaction((tx)=> {
            tx.executeSql(insert, [content.name, content.desc, content.img],
                ()=>{console.log('inseriu com sucesso')}, (error)=>{console.log('erro ao inserir ', error)})
        });  
    }

    updateContent(id, content, db) {
        return new Promise((resolve) => {
            db.transaction((tx) => {
                    tx.executeSql(update, [content.name, content.desc, content.img, id],
                        (_, { rows }) => {resolve(rows)})
                });
        });
    }

    deleteContent(id, db) {
        return new Promise((resolve) => {
            db.transaction((tx) => {
                    tx.executeSql(DELETE, [id],
                        (_, { rows }) => {resolve(rows)})
                });
        });
    }

}