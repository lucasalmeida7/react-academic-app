import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';

const ContentItem = (props) => {
    const {name, img, onPress} = props;

    return (
        <TouchableOpacity onPress={()=>onPress()}>
            <Text>{name}</Text>
        </TouchableOpacity>    
    )

}

export default ContentItem;