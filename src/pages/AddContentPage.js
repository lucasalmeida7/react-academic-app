
import React, {Component} from 'react'
import {ScrollView, TextInput, Button} from 'react-native'
import Database from '../database/Database';

const database = new Database();
let db;

export default class AddContentPage extends Component {
    constructor(){
        super();
        this.state = {
            name: '',
            desc: '',
            img: ''
        };
    }

    componentDidMount(){
        db = this.props.navigation.getParam('db');
    }
    
    updateTextInput(value, field){
        const state = this.state;
        state[field] = value;
        this.setState(state);
    }

    saveContent(){
        const content = {
            name: this.state.name,
            desc: this.state.desc,
            img: this.state.img
        }

        database.addContent(content, db);

        this.props.navigation.goBack();
    }

    render() {
        return (
            <ScrollView>
                <TextInput placeholder={'Nome'}
                    value={this.state.name}
                    onChangeText={(value)=> this.updateTextInput(value, 'name')}>
                </TextInput>
                <TextInput placeholder={'Descrição'}
                    value={this.state.desc}
                    onChangeText={(value)=> this.updateTextInput(value, 'desc')}>
                </TextInput>
                <TextInput placeholder={'Imagem'}
                    value={this.state.img}
                    onChangeText={(value)=> this.updateTextInput(value, 'img')}>
                </TextInput>
                <Button
                    title='Salvar'
                    onPress={()=>this.saveContent()} >    
                </Button>
            </ScrollView>
        )
    }

}