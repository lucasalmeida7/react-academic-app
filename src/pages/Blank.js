import React from 'react';
import {View, Text} from 'react-native';

class Blank extends React.Component
{
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View>
                <Text>Email: { this.props.navigation.state.params.user.mail } </Text>
                <Text>Senha: { this.props.navigation.state.params.user.pass } </Text>
            </View>
        );
    }
}

export default Blank;