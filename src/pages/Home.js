import React from 'react';
import {View, Text, StyleSheet } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import ContentItem from '../components/ContentItem';

class Home extends React.Component{
    constructor(props) {
        super(props);
    }

    render() {
        const content = [
            'Análise de Riscos',
            'Dispositivos Móveis',
            'Engenharia de Software Experimental',
            'Gerencia de Projetos',
            'Seminários III'
        ]

        const items = content.map((name, index) => 
            <ContentItem key={index} name={name} />
        )

        return (
            <ScrollView>
                {items}
            </ScrollView>
        );
    }
}

export default Home;