import React from 'react';
import { ScrollView } from 'react-native';
import ContentItem from '../components/ContentItem';
import { Fab, View, Text } from '../styles/style';

import Database from '../database/Database';

const database = new Database();
let db;

class ContentPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      contents: []
    }  
  }

  componentDidMount() {
    db = database.initDB();
    const didFocusSubscribe = this.props.navigation.addListener(
      'didFocus',
      () => {
        this.listContents();
      }
    );
  }
  
  listContents() {
    database.listContents(db).then((contents) => {
      this.setState({
        contents
      })
    });
  }

  render(){
      const { contents } = this.state;

      const items = contents.map((content) =>
        <ContentItem key={content.id} name={content.name} img={content.img}
          onPress={() => {
            console.log('clicou no item ',content.id );
            this.props.navigation.navigate('ContentDetail', {
              db:db, id:content.id
            });
          }}
          />
      )

      return (
        <View>
          <ScrollView>
            {items}
          </ScrollView>
          <Fab onPress={
            ()=>{ this.props.navigation.navigate('AddContent', {db:db})}
          }>
            <Text color='white' fontSize='30px'>+</Text>
          </Fab>
        </View>
      )

      }
}
export default ContentPage;