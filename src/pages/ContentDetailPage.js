
import React, {Component} from 'react'
import {ScrollView, Text, Button} from 'react-native'
import Database from '../database/Database';

const database = new Database();
let db;

export default class ContentDetailPage extends Component {
    constructor(){
        super();
        this.state = {
            id: '',
            name: '',
            desc: '',
            img: ''
        };
    }

    componentDidMount(){
        db = this.props.navigation.getParam('db');
        const id = this.props.navigation.getParam('id');
        
        database.contentById(id, db).then((data)=>{
            this.setState({
                id: id,
                name: data.name,
                desc: data.desc,
                img: data.img
            })
        })
    }
    
    deleteContent(){
        const id = this.state.id;
        database.deleteContent(id, db);
        this.props.navigation.goBack();
    }

    render() {
        return (
            <ScrollView>
                <Text>ID: {this.state.id}</Text>
                <Text>Nome: {this.state.name}</Text>
                <Text>Descrição: {this.state.desc}</Text>
                <Text>Imagem: {this.state.img}</Text>
                <Button
                    title='Apagar'
                    onPress={()=>this.deleteContent()} />    
            </ScrollView>
        )
    }

}