import Login from './pages/Login';
import ContentPage from './pages/ContentPage';
import AddContentPage from './pages/AddContentPage';
import ContentDetailPage from './pages/ContentDetailPage';

import { createAppContainer } from 'react-navigation';
import { createStackNavigator} from 'react-navigation-stack';

const AppNavigator = createStackNavigator(
  {
    'Content' : {
      screen: ContentPage,
      navigationOptions: {
        title: "Conteudo"
      }
    },
    'AddContent' : {
      screen: AddContentPage,
      navigationOptions: {
        title: "Adicionar"
      }
    },
    'ContentDetail' : {
      screen: ContentDetailPage,
      navigationOptions: {
        title: "Detalhes"
      }
    },
    'Login' : {
      screen: Login,
    }
  }, {
    defaultNavigationOptions: {
      title: "Login",
      headerStyle: {
        backgroundColor:"#06f"
      },
      headerTitleStyle: {
        color: 'white',
        fontSize: 30,
      }
    }
  }
);

const Router = createAppContainer(AppNavigator); 
export default Router;
