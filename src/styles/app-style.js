
import styled from "styled-components/native";

export const View = styled.View`
    margin-top: 20px;
    background-color: '#fff';
    display: flex;
    flex-direction: column;
    flex: 1;

    padding-top: ${props => props.paddingTop || 0 };
    padding-bottom: ${props => props.paddingBottom || 0 };
    padding-left: ${props => props.paddingLeft || 0 };
    padding-right: ${props => props.paddingRight || 0 };

`;

export const TextInput = styled.TextInput`
    border-color: '#000000'
    border-bottom-width: 1px;
    padding-bottom: 10px;
    padding-right: 10px;
    padding-left: 10px;
    font-size: 30px;
`;