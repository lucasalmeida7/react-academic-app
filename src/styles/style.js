import styled from "styled-components/native";

export const View = styled.View`
    display: flex;
    flex-direction: column;
    flex: 1;
    background: #ffffff;
    margin-top: 20px;
    padding-left: ${props => props.paddingLeft || 0};
    padding-right: ${props => props.paddingRight || 0};
    padding-top: ${props => props.paddingTop || 0};
`;

export const TextInput = styled.TextInput`
    border-color: #000000;
    border-bottom-width: 1px;
    padding-bottom: 10px;
    padding-right: 5px;
    padding-left: 5px;
    font-size: 30px;
`;

export const Fab = styled.TouchableOpacity`
    height: 50px;
    width: 50px;
    border-radius: 200px;
    position: absolute; 
    bottom: 20px;
    right: 20px;
    justify-content: center;
    align-items: center;
    background: #06f;
`;

export const Text = styled.Text`
    font-size: ${props => props.fontSize || '20px' };
    color: ${props => props.color || 'black' };
`;